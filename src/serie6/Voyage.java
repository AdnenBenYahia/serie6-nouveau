package serie6;

import java.util.ArrayList;
import java.util.List;


public class Voyage 
{
 String portDep;
 String portDest;
 int nbreJours;
 
 //Question 1: la m�thode Get (lecture)
 public String getPortDep()
 {
   return portDep;
 }
 public String getPortDest()
 {
   return portDest;
 }
public int getNbreJours()
{
 return nbreJours;
}
//Question2
 @Override
 public boolean equals(Object obj) 
 {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Voyage other = (Voyage) obj;
	if (nbreJours != other.nbreJours)
		return false;
	if (portDep == null) {
		if (other.portDep != null)
			return false;
	} 
	else if (!portDep.equals(other.portDep))
		return false;
    else if (!portDep.equals(other.portDest))
		return false;
	if (portDest == null) {
		if (other.portDest != null)
			return false;
	} 
	else if (!portDest.equals(other.portDest))
		return false;
    else if (!portDest.equals(other.portDep))
		return false;
	
	return true;
 }
 @Override
 public int hashCode() 
 {
	final int prime = 31;
	int result = 1;
	result = prime * result + nbreJours;
	result = prime * result + ((portDep == null) ? 0 : portDep.hashCode());
	result = prime * result + ((portDest == null) ? 0 : portDest.hashCode());
	return result;
 }
 
 static List <Voyage> voyages=new ArrayList<Voyage>();

 public Voyage(String depart, String destination, int NJours)
 {
	portDep = depart;
	portDest=destination;
	nbreJours=NJours;
	voyages.add(this);
 }
 public List<Voyage> getVoyage(String depart, String destination)
 {
     return voyages;
 }
@Override
public String toString() 
{
	return "Voyage [portDep=" + portDep + ", portDest=" + portDest + ", nbreJours=" + nbreJours + "]";
}
 //Question 5
 public static List<Voyage> getVoyageAuDepartDe(String Dep)
 {
   List<Voyage> nouveauVoyages = new ArrayList<Voyage>();
  for (Voyage v:voyages)
  {  
    if(v.getPortDep().equals(Dep)) 
    	nouveauVoyages.add(v);
  }
  return nouveauVoyages;
 }
 //Question 6
 public static Voyage getPlusCourtAuDepartDe(String Dep)
 {
   Voyage voy = null;
   List<Voyage> voyages = new ArrayList<Voyage>();
   voyages=  Voyage.getVoyageAuDepartDe(Dep);
  int min=1000;//on se donne une valeur tr�s grande pour qu'elle soit modifi�e lors de la comparaison
  for (Voyage v:voyages)
  {
    if(v.getNbreJours()<min)
    {
     min=v.getNbreJours();
    }
  }
  for (Voyage v:voyages)
  {
	if (min==v.getNbreJours())  
	{
	  voy=v;
	}
  }
return voy;
 }
 //Question 7
 public static Voyage newInstance(String depart ,String arrivee ,int nombreJours)
 {
   Voyage voy = null;
   List<Voyage> voyages = new ArrayList<Voyage>();
   voyages= Voyage.getVoyageAuDepartDe(depart);
   for (Voyage v:voyages)
   {
	 if(depart.equals(v.getPortDep())&& arrivee.equals(v.getPortDest()) && nombreJours==v.getNbreJours())
	 {
	   voy=v;
	 }
   }
   if(voy==null)
   {voy=new Voyage(depart,arrivee,nombreJours);}
   return voy;
 }
 //Question 8
 

}
